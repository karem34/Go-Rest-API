package routers

import (
	"github.com/gorilla/mux"
	"gitlab.com/greenly/go-rest-api/controllers"
	"gitlab.com/greenly/go-rest-api/utils/middlewares"
)

// CreateRouter for handle all requests
func CreateRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	// Enable CORS for all endpoints
	router.Use(middlewares.CORSMiddleWare)
	// Show request URL
	router.Use(middlewares.URLMiddleWare)
	// Basic Authentication middleware
	//router.Use(authMiddleWare)
	// JWT Authentication middleware
	router.Use(middlewares.JWTMiddleWare)
	// Route for all end points to skip OPTIONS method for CORS
	router.HandleFunc("/articles", controllers.SkipCORS).Methods("OPTIONS")
	router.HandleFunc("/users", controllers.SkipCORS).Methods("OPTIONS")
	router.HandleFunc("/users/login", controllers.SkipCORS).Methods("OPTIONS")
	router.HandleFunc("/users/new", controllers.SkipCORS).Methods("OPTIONS")
	router.HandleFunc("/comments", controllers.SkipCORS).Methods("OPTIONS")
	router.HandleFunc("/likes", controllers.SkipCORS).Methods("OPTIONS")
	router.HandleFunc("/agrees", controllers.SkipCORS).Methods("OPTIONS")
	// Router for / end point
	router.HandleFunc("/", controllers.HomePage)
	// Routers for /article... end point
	articlesRouter := router.PathPrefix("/articles").Subrouter()
	articlesRouter.HandleFunc("", controllers.ReturnAllArticles).Methods("GET")
	articlesRouter.HandleFunc("", controllers.CreateNewArticle).Methods("POST")
	articlesRouter.HandleFunc("/{id}", controllers.ReturnSingleArticle).Methods("GET")
	articlesRouter.HandleFunc("/{id}", controllers.DeleteSingleArticle).Methods("DELETE")
	articlesRouter.HandleFunc("/{id}", controllers.UpdateSingleArticle).Methods("PUT")
	// Routers for /user... end point
	usersRouter := router.PathPrefix("/users").Subrouter()
	usersRouter.HandleFunc("", controllers.ReturnAllUsers).Methods("GET")
	usersRouter.HandleFunc("/login", controllers.LoginUser).Methods("POST")
	usersRouter.HandleFunc("/new", controllers.CreateNewUser).Methods("POST")
	usersRouter.HandleFunc("/{id}", controllers.ReturnSingleUser).Methods("GET")
	usersRouter.HandleFunc("/{id}", controllers.DeleteSingleUser).Methods("DELETE")
	usersRouter.HandleFunc("/{id}", controllers.UpdateSingleUser).Methods("PUT")
	// Routers for /comment... end point
	commentsRouter := router.PathPrefix("/comments").Subrouter()
	commentsRouter.HandleFunc("", controllers.ReturnAllComments).Methods("GET")
	commentsRouter.HandleFunc("", controllers.CreateNewComment).Methods("POST")
	commentsRouter.HandleFunc("/{id}", controllers.ReturnSingleComment).Methods("GET")
	commentsRouter.HandleFunc("/{id}", controllers.DeleteSingleComment).Methods("DELETE")
	commentsRouter.HandleFunc("/{id}", controllers.UpdateSingleComment).Methods("PUT")
	// Routers for /comment/{id}/replies... end point
	commentRepliesRouter := commentsRouter.PathPrefix("/{id}/replies").Subrouter()
	commentRepliesRouter.HandleFunc("", controllers.ReturnAllCommentReplies).Methods("GET")
	commentRepliesRouter.HandleFunc("", controllers.CreateNewCommentReply).Methods("POST")
	commentRepliesRouter.HandleFunc("/{rd}", controllers.ReturnSingleCommentReply).Methods("GET")
	commentRepliesRouter.HandleFunc("/{rd}", controllers.DeleteSingleCommentReply).Methods("DELETE")
	// Routers for /like... end point
	likesRouter := router.PathPrefix("/likes").Subrouter()
	likesRouter.HandleFunc("", controllers.ReturnAllLikes).Methods("GET")
	likesRouter.HandleFunc("", controllers.CreateNewLike).Methods("POST")
	likesRouter.HandleFunc("/{id}", controllers.ReturnSingleLike).Methods("GET")
	likesRouter.HandleFunc("/{id}", controllers.DeleteSingleLike).Methods("DELETE")
	likesRouter.HandleFunc("/{id}", controllers.UpdateSingleLike).Methods("PUT")
	// Routers for /agree... end point
	agreesRouter := router.PathPrefix("/agrees").Subrouter()
	agreesRouter.HandleFunc("", controllers.ReturnAllAgrees).Methods("GET")
	agreesRouter.HandleFunc("", controllers.CreateNewAgree).Methods("POST")
	agreesRouter.HandleFunc("/{id}", controllers.ReturnSingleAgree).Methods("GET")
	agreesRouter.HandleFunc("/{id}", controllers.DeleteSingleAgree).Methods("DELETE")
	agreesRouter.HandleFunc("/{id}", controllers.UpdateSingleAgree).Methods("PUT")
	return router
}
